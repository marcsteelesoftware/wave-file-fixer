package uk.co.dlineradio.wavefilefixer.chunks;

import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

import uk.co.dlineradio.wavefilefixer.WAVEChunk;
import uk.co.dlineradio.wavefilefixer.util.ByteUtil;

/**
 * A data chunk in a WAVE file.
 * @author Marc Steele
 */

public class DataChunk extends WAVEChunk {
	
	private byte[] data = null;
	private static final String CHUNK_PREFIX_DATA = "data";
	private static Logger logger = Logger.getLogger(DataChunk.class.getName());
	
	/**
	 * Generates a data chunk from the raw binary data.
	 * @param data The data.
	 * @return
	 */
	
	public static DataChunk fromBytes(byte[] data) {
		
		// Sanity check
		
		if (data == null || data.length == 0) {
			logger.log(Level.SEVERE, "Could not create the chunk as no data was supplied.");
			return null;
		}
		
		// Create the new chunk
		
		DataChunk new_chunk = new DataChunk();
		new_chunk.data = data;
		return new_chunk;
		
	}

	@Override
	public byte[] toBytes() {
		
		// Sanity check
		
		if (this.data == null || this.data.length == 0) {
			logger.log(Level.SEVERE, "Could not generate block data as there is no block data.");
			return null;
		}
		
		// Do the conversion
		
		ByteBuffer buffer = ByteBuffer.allocate(HEADER_TOTAL_LENGTH + this.data.length);
		
		buffer.put(ByteUtil.stringToByteArrayBigEndian(CHUNK_PREFIX_DATA));
		buffer.put(ByteUtil.intToByteArrayLittleEndian(this.data.length));
		buffer.put(this.data);
		
		return buffer.array();
		
	}

	@Override
	public String toString() {

		if (this.data != null) {
			return String.format("Data chunk of length %1$d bytes.\n\n", this.data.length);
		} else {
			return "Data chunk of unknown length.\n\n";
		}

	}

}
