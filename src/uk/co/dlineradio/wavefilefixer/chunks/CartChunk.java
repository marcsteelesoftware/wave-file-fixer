package uk.co.dlineradio.wavefilefixer.chunks;

import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import uk.co.dlineradio.wavefilefixer.WAVEChunk;
import uk.co.dlineradio.wavefilefixer.util.ByteUtil;

/**
 * The cart chunk in a WAVE file.
 * @author Marc Steele
 */

public class CartChunk extends WAVEChunk {
	
	private String versionMajor;
	private String versionMinor;
	private String title;
	private String artist;
	private String cutID;
	private String clientID;
	private String category;
	private String classification;
	private String outCue;
	private Date startDate;
	private Date endDate;
	private String producerAppID;
	private String producerAppVersion;
	private String userDefinedText;
	private int zeroDBRef;
	private String tagText;
	private Map<String, CartTimer> timers = new HashMap<String, CartTimer>();
	
	private static SimpleDateFormat cartChunkDateFormat = new SimpleDateFormat("yyyy/MM/ddHH:mm:ss");
	private static SimpleDateFormat displayDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	private static Logger logger = Logger.getLogger(CartChunk.class.getName());

	private static final String CHUNK_PREFIX_CART = "cart";
	private static final String DEFAULT_PRODUCER_APP_ID = "Wave File Fixer";
	private static final String DEFAULT_PRODUCER_APP_VERSION = "1.0";
	private static final String DEFAULT_VERSION_MAJOR = "01";
	private static final String DEFAULT_VERSION_MINOR = "01";
	
	private static final int TIMER_COUNT = 8;
	private static final int TIMER_LENGTH = 8;
	private static final int MINIMUM_CHUNK_LENGTH = 1024;
	private static final int INDEX_VERSION_MAJOR = 0;
	private static final int LENGTH_VERSION_MAJOR = 2;
	private static final int INDEX_VERSION_MINOR = 2;
	private static final int LENGTH_VERSION_MINOR = 2;
	private static final int INDEX_TITLE = 4;
	private static final int LENGTH_TITLE = 64;
	private static final int INDEX_ARTIST = 68;
	private static final int LENGTH_ARTIST = 64;
	private static final int INDEX_CUT_ID = 132;
	private static final int LENGTH_CUT_ID = 64;
	private static final int INDEX_CLIENT_ID = 196;
	private static final int LENGTH_CLIENT_ID = 64;
	private static final int INDEX_CATEGORY = 260;
	private static final int LENGTH_CATEGORY = 64;
	private static final int INDEX_CLASSIFICATION = 324;
	private static final int LENGTH_CLASSIFICATION = 64;
	private static final int INDEX_OUT_CUE = 388;
	private static final int LENGTH_OUT_CUE = 64;
	private static final int INDEX_START_DATE = 452;
	private static final int LENGTH_START_DATE = 18;
	private static final int INDEX_END_DATE = 470;
	private static final int LENGTH_END_DATE = 18;
	private static final int INDEX_PRODUCER_APP_ID = 488;
	private static final int LENGTH_PRODUCER_APP_ID = 64;
	private static final int INDEX_PRODUCER_APP_VERSION = 552;
	private static final int LENGTH_PRODUCER_APP_VERSION = 64;
	private static final int INDEX_USER_DEFINED_TEXT = 616;
	private static final int LENGTH_USER_DEFINED_TEXT = 64;
	private static final int INDEX_ZERO_DB_REF = 680;
	private static final int LENGTH_ZERO_DB_REF = 4;
	private static final int INDEX_TIMERS = 684;
	private static final int LENGTH_TIMERS = 64;
	private static final int INDEX_RESERVED = 748;
	private static final int LENGTH_RESERVED = 276;
	private static final int INDEX_TAG_TEXT = 1024;
	
	/**
	 * Generates a cart chunk from the byte array of a file.
	 * @param data The data for this chunk.
	 * @return The cart chunk if we could parse it. Otherwise NULL.
	 */
	
	public static CartChunk fromBytes(byte[] data) {
		
		// Sanity check
		
		if (data == null || data.length < MINIMUM_CHUNK_LENGTH) {
			logger.log(Level.SEVERE, "Failed to parse the format chunk as it's not of the right length.");
			return null;
		}
		
		// Inflate the chunk
		
		CartChunk chunk = new CartChunk();
		
		chunk.versionMajor = ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, INDEX_VERSION_MAJOR, INDEX_VERSION_MAJOR + LENGTH_VERSION_MAJOR), true);
		chunk.versionMinor = ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, INDEX_VERSION_MINOR, INDEX_VERSION_MINOR + LENGTH_VERSION_MINOR), true);
		chunk.title = ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, INDEX_TITLE, INDEX_TITLE + LENGTH_TITLE), true);
		chunk.artist = ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, INDEX_ARTIST, INDEX_ARTIST + LENGTH_ARTIST), true);
		chunk.cutID = ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, INDEX_CUT_ID, INDEX_CUT_ID + LENGTH_CUT_ID), true);
		chunk.clientID = ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, INDEX_CLIENT_ID, INDEX_CLIENT_ID + LENGTH_CLIENT_ID), true);
		chunk.category = ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, INDEX_CATEGORY, INDEX_CATEGORY + LENGTH_CATEGORY), true);
		chunk.classification = ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, INDEX_CLASSIFICATION, INDEX_CLASSIFICATION + LENGTH_CLASSIFICATION), true);
		chunk.outCue = ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, INDEX_OUT_CUE, INDEX_OUT_CUE + LENGTH_OUT_CUE), true);
		chunk.producerAppID = ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, INDEX_PRODUCER_APP_ID, INDEX_PRODUCER_APP_ID + LENGTH_PRODUCER_APP_ID), true);
		chunk.producerAppVersion = ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, INDEX_PRODUCER_APP_VERSION, INDEX_PRODUCER_APP_VERSION + LENGTH_PRODUCER_APP_VERSION), true);
		chunk.userDefinedText = ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, INDEX_USER_DEFINED_TEXT, INDEX_USER_DEFINED_TEXT + LENGTH_USER_DEFINED_TEXT), true);
		chunk.zeroDBRef = ByteUtil.byteToIntLittleEndian(Arrays.copyOfRange(data, INDEX_ZERO_DB_REF, INDEX_ZERO_DB_REF + LENGTH_ZERO_DB_REF));
		
		if (data.length > MINIMUM_CHUNK_LENGTH) {
			chunk.tagText = ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, INDEX_TAG_TEXT, data.length - 1), true);
		}
		
		// Dates
		
		String startDateText = ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, INDEX_START_DATE, INDEX_START_DATE + LENGTH_START_DATE), true);
		String endDateText = ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, INDEX_END_DATE, INDEX_END_DATE + LENGTH_END_DATE), true);
		
		try {
		
			chunk.startDate = cartChunkDateFormat.parse(startDateText);
			chunk.endDate = cartChunkDateFormat.parse(endDateText);
			
		} catch (Exception ex) {
			logger.log(Level.SEVERE, "The start and/or end date is malformatted. Defaulting to the ends of time", ex);
			chunk.startDate = new Date(0);
			chunk.endDate = new Date(Long.MAX_VALUE);
		}
		
		// Timers
		
		byte[] timerData = Arrays.copyOfRange(data, INDEX_TIMERS, INDEX_TIMERS + LENGTH_TIMERS);
		for (int i = 0; i < TIMER_COUNT; i++) {
			
			byte[] currentTimerData = Arrays.copyOfRange(timerData, i * TIMER_LENGTH, (i + 1) * TIMER_LENGTH);
			CartTimer currentTimer = CartTimer.fromBytes(currentTimerData);
			if (currentTimer != null) {
				chunk.timers.put(currentTimer.getType(), currentTimer);
			}
			
		}
		
		return chunk;
		
	}

	@Override
	public byte[] toBytes() {
		
		// Work out the length we need then generate the destination buffer
		
		int length = MINIMUM_CHUNK_LENGTH;
		
		if (this.tagText != null && !this.tagText.isEmpty()) {
			length += this.tagText.length() + 1;
		}
		
		ByteBuffer buffer = ByteBuffer.allocate(HEADER_TOTAL_LENGTH + length);
		
		// Header
		
		buffer.put(ByteUtil.stringToByteArrayBigEndian(CHUNK_PREFIX_CART));
		buffer.put(ByteUtil.intToByteArrayLittleEndian(length));
		
		// Content
		
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.stringToByteArrayBigEndian(DEFAULT_VERSION_MAJOR), LENGTH_VERSION_MAJOR));
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.stringToByteArrayBigEndian(DEFAULT_VERSION_MINOR), LENGTH_VERSION_MAJOR));
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.stringToByteArrayBigEndian(this.title), LENGTH_TITLE));
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.stringToByteArrayBigEndian(this.artist), LENGTH_ARTIST));
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.stringToByteArrayBigEndian(this.cutID), LENGTH_CUT_ID));
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.stringToByteArrayBigEndian(this.clientID), LENGTH_CLIENT_ID));
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.stringToByteArrayBigEndian(this.category), LENGTH_CATEGORY));
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.stringToByteArrayBigEndian(this.classification), LENGTH_CLASSIFICATION));
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.stringToByteArrayBigEndian(this.outCue), LENGTH_OUT_CUE));
		
		// Dates
		
		if (this.startDate == null || this.endDate == null) {
			logger.log(Level.WARNING, "Could not generate the cart chunk as we had a NULL start and/or end date and time");
			return null;
		} else {
			
			String startDateText = cartChunkDateFormat.format(this.startDate);
			String endDateText = cartChunkDateFormat.format(this.endDate);
			buffer.put(ByteUtil.safeCopyOf(ByteUtil.stringToByteArrayBigEndian(startDateText), LENGTH_START_DATE));
			buffer.put(ByteUtil.safeCopyOf(ByteUtil.stringToByteArrayBigEndian(endDateText), LENGTH_END_DATE));
			
		}
		
		// More generic data
		
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.stringToByteArrayBigEndian(DEFAULT_PRODUCER_APP_ID), LENGTH_PRODUCER_APP_ID));
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.stringToByteArrayBigEndian(DEFAULT_PRODUCER_APP_VERSION), LENGTH_PRODUCER_APP_VERSION));
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.stringToByteArrayBigEndian(this.userDefinedText), LENGTH_USER_DEFINED_TEXT));
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.intToByteArrayLittleEndian(this.zeroDBRef), LENGTH_ZERO_DB_REF));
		
		// Timers
		
		if (timers.size() > TIMER_COUNT) {
			logger.log(Level.SEVERE, String.format("Could not generate the cart chunk data as we have %1$d timers. The standard only allows %2$d timers.", this.timers.size(), TIMER_COUNT));
			return null;
		} else {
			
			// Fill with existing timers
			
			int currentTimerCount = 0;
			
			for (CartTimer currentTimer:this.timers.values()) {
				buffer.put(currentTimer.toBytes());
				currentTimerCount++;
			}
			
			// Pad out
			
			while (currentTimerCount < TIMER_COUNT) {
				currentTimerCount++;
				byte[] emptyTimer = new byte[8];
				Arrays.fill(emptyTimer, (byte) 0);
				buffer.put(emptyTimer);
			}
			
		}
		
		// Reserved and tag extend bit
		
		byte[] reservedData = new byte[LENGTH_RESERVED];
		Arrays.fill(reservedData, (byte) 0);
		buffer.put(reservedData);
		
		if (this.tagText != null && !this.tagText.isEmpty()) {
			buffer.put(ByteUtil.safeCopyOf(ByteUtil.stringToByteArrayBigEndian(this.tagText), this.tagText.length()));
			buffer.put((byte) 0);
		}
		
		return buffer.array();
		
	}

	@Override
	public String toString() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("Cart Chunk\n");
		
		sb.append(String.format("Version: %1$s.%2$s\n", this.versionMajor, this.versionMinor));
		sb.append(String.format("Title: %1$s\n", this.title));
		sb.append(String.format("Artist: %1$s\n", this.artist));
		sb.append(String.format("Cut ID: %1$s\n", this.cutID));
		sb.append(String.format("Client ID: %1$s\n", this.clientID));
		sb.append(String.format("Category: %1$s\n", this.category));
		sb.append(String.format("Classification: %1$s\n", this.classification));
		sb.append(String.format("Out Cue: %1$s\n", this.outCue));
		sb.append(String.format("Start Date: %1$s\n", displayDateFormat.format(this.startDate)));
		sb.append(String.format("End Date: %1$s\n", displayDateFormat.format(this.endDate)));
		sb.append(String.format("Producer Application: %1$s (Version %2$s)\n", this.producerAppID, this.producerAppVersion));
		sb.append(String.format("User Defined Text: %1$s\n", this.userDefinedText));
		sb.append(String.format("0dB Reference: %1$d\n", this.zeroDBRef));
		sb.append(String.format("Tag Text: %1$s\n", this.tagText));
		
		for (CartTimer currentTimer:this.timers.values()) {
			sb.append(String.format("Cart Timer: %1$s\n", currentTimer));
		}
		
		sb.append("\n");
		
		return sb.toString();
		
	}

	/**
	 * Obtains the major version number of the cart chunk.
	 * @return The major version number.
	 */
	
	public String getVersionMajor() {
		return versionMajor;
	}
	
	/**
	 * Sets the major version number of the cart chunk.
	 * @param versionMajor The major version number.
	 */

	public void setVersionMajor(String versionMajor) {
		this.versionMajor = versionMajor;
	}
	
	/**
	 * Obtains the minor version number of the cart chunk.
	 * @return The minor version number.
	 */

	public String getVersionMinor() {
		return versionMinor;
	}
	
	/**
	 * Sets the minor version number of the cart chunk.
	 * @param versionMinor The minor version number.
	 */

	public void setVersionMinor(String versionMinor) {
		this.versionMinor = versionMinor;
	}
	
	/**
	 * Obtains the title of the cart.
	 * @return The title.
	 */

	public String getTitle() {
		return title;
	}
	
	/**
	 * Sets the title of the cart.
	 * @param title The title.
	 */

	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * Obtains the artist of the cart.
	 * @return The artist.
	 */

	public String getArtist() {
		return artist;
	}
	
	/**
	 * Sets the artist of the cart.
	 * @param artist The artist.
	 */

	public void setArtist(String artist) {
		this.artist = artist;
	}

	/**
	 * Obtains the cut ID of the cart.
	 * @return The cut ID.
	 */
	
	public String getCutID() {
		return cutID;
	}
	
	/**
	 * Sets the cut ID of the cart.
	 * @param cutID The cut ID.
	 */

	public void setCutID(String cutID) {
		this.cutID = cutID;
	}
	
	/**
	 * Obtains the client ID of the cart.
	 * @return The client ID.
	 */

	public String getClientID() {
		return clientID;
	}

	/**
	 * Sets the client ID of the cart.
	 * @param clientID The client ID.
	 */
	
	public void setClientID(String clientID) {
		this.clientID = clientID;
	}
	
	/**
	 * Obtains the category for the cart.
	 * @return The category.
	 */

	public String getCategory() {
		return category;
	}
	
	/**
	 * Sets the category for the cart.
	 * @param category The category.
	 */

	public void setCategory(String category) {
		this.category = category;
	}
	
	/**
	 * Obtains the classification of the cart.
	 * @return The classification.
	 */

	public String getClassification() {
		return classification;
	}
	
	/**
	 * Sets the classification of the cart.
	 * @param classification The classification.
	 */

	public void setClassification(String classification) {
		this.classification = classification;
	}
	
	/**
	 * Obtains the out cue for the cart.
	 * @return The out cue.
	 */

	public String getOutCue() {
		return outCue;
	}
	
	/**
	 * Sets the out cue for the cart.
	 * @param outCue The out cue.
	 */

	public void setOutCue(String outCue) {
		this.outCue = outCue;
	}

	/**
	 * Obtains the start date for the cart.
	 * @return The start date.
	 */
	
	public Date getStartDate() {
		return startDate;
	}
	
	/**
	 * Sets the start date for the cart.
	 * @param startDate The start date.
	 */

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	/**
	 * Obtains the end date for the cart.
	 * @return The end date.
	 */

	public Date getEndDate() {
		return endDate;
	}
	
	/**
	 * Sets the end date for the cart.
	 * @param endDate The end date.
	 */

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	/**
	 * Obtains the producer application for the cart.
	 * @return The producer application.
	 */

	public String getProducerAppID() {
		return producerAppID;
	}
	
	/**
	 * Sets the producer application for the cart.
	 * @param producerAppID The producer application.
	 */

	public void setProducerAppID(String producerAppID) {
		this.producerAppID = producerAppID;
	}
	
	/**
	 * Obtains the producer application version for the cart.
	 * @return The producer application version.
	 */

	public String getProducerAppVersion() {
		return producerAppVersion;
	}
	
	/**
	 * Sets the producer application version for the cart.
	 * @param producerAppVersion The producer application version.
	 */

	public void setProducerAppVersion(String producerAppVersion) {
		this.producerAppVersion = producerAppVersion;
	}
	
	/**
	 * Obtains the free-form user defined text for the cart.
	 * @return The free-form user defined text.
	 */

	public String getUserDefinedText() {
		return userDefinedText;
	}
	
	/**
	 * Sets the free-form user defined text for the cart.
	 * @param userDefinedText The free-form user defined text.
	 */

	public void setUserDefinedText(String userDefinedText) {
		this.userDefinedText = userDefinedText;
	}
	
	/**
	 * Obtains the 0dB reference for the audio.
	 * @return The 0dB reference.
	 */

	public int getZeroDBRef() {
		return zeroDBRef;
	}
	
	/**
	 * Sets the 0dB reference for the audio.
	 * @param zeroDBRef The 0dB reference.
	 */

	public void setZeroDBRef(int zeroDBRef) {
		this.zeroDBRef = zeroDBRef;
	}

	/**
	 * Obtains the tag text for the cart. This is an optional field.
	 * @return The tag text.
	 */
	
	public String getTagText() {
		return tagText;
	}
	
	/**
	 * Sets the tag text for the cart. This is an optional field.
	 * @param tagText The tag text.
	 */

	public void setTagText(String tagText) {
		this.tagText = tagText;
	}
	
	/**
	 * Retrieves the cart timers for manipulation.
	 * @return The cart timers.
	 */

	public Map<String, CartTimer> getTimers() {
		return timers;
	}

}
