package uk.co.dlineradio.wavefilefixer.chunks;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import uk.co.dlineradio.wavefilefixer.util.ByteUtil;

/**
 * A timer in the cart chunk file.
 * @author Marc Steele
 */

public class CartTimer {
	
	private String type;
	private int value;
	
	private static Logger logger = Logger.getLogger(CartTimer.class.getName());
	
	private static final int TOTAL_LENGTH = 8;
	private static final int INDEX_TYPE = 0;
	private static final int LENGTH_TYPE = 4;
	private static final int INDEX_VALUE = 4;
	private static final int LENGTH_VALUE = 4;
	
	/**
	 * Generates a timer from the raw data.
	 * @param data The data representing the timer.
	 * @return Assuming all went well, the timer. Otherwise NULL.
	 */
	
	public static CartTimer fromBytes(byte[] data) {
		
		// Sanity check
		
		if (data == null || data.length < TOTAL_LENGTH) {
			logger.log(Level.SEVERE, "Could not inflate timer as not enough data was supplied.");
			return null;
		}
		
		// Check for all zeros
		
		boolean allZeros = true;
		
		for (int i = 0; i < data.length; i++) {
			if (data[i] != 0) {
				allZeros = false;
			}
		}
		
		if (allZeros) {
			return null;
		}
		
		// Perform the inflation
		
		CartTimer timer = new CartTimer();
		timer.type = ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, INDEX_TYPE, INDEX_TYPE + LENGTH_TYPE), true);
		timer.value = ByteUtil.byteToIntLittleEndian(Arrays.copyOfRange(data, INDEX_VALUE, INDEX_VALUE + LENGTH_VALUE));
		
		// Check it
		
		if (timer.type == null || timer.type.isEmpty()) {
			return null;
		}
		
		return timer;
		
	}
	
	/**
	 * Generates a byte array representation of the timer.
	 * @return The byte array representation.
	 */
	
	public byte[] toBytes() {
		
		ByteBuffer buffer = ByteBuffer.allocate(TOTAL_LENGTH);
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.stringToByteArrayBigEndian(this.type), LENGTH_TYPE));
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.intToByteArrayLittleEndian(this.value), LENGTH_VALUE));
		return buffer.array();
		
	}
	
	/**
	 * Obtains the type of timer this is.
	 * @return The type of timer.
	 */
	
	public String getType() {
		return type;
	}
	
	/**
	 * Sets the type of timer this is.
	 * @param type The type of timer.
	 */
	
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * Obtains the value for the timer.
	 * @return The value.
	 */
	
	public int getValue() {
		return value;
	}
	
	/**
	 * Sets the value for the timer.
	 * @param value The value.
	 */
	
	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return String.format("%1$s at %2$d samples", this.type, this.value);
	}

}
