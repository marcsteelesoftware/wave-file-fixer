package uk.co.dlineradio.wavefilefixer.chunks;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import uk.co.dlineradio.wavefilefixer.WAVEChunk;
import uk.co.dlineradio.wavefilefixer.util.ByteUtil;

/**
 * The file format chunk in a WAVE file.
 * @author Marc Steele
 */

public class FormatChunk extends WAVEChunk {
	
	/**
	 * Describes the wave format a file has.
	 * @author Marc Steele
	 */
	
	public enum WaveFormat {
		
		Unknown,
		PCM,
		ADPCM,
		G711_ALaw,
		G711_MuLaw,
		IMA_ADPCM,
		G723_ADPCM,
		GSM,
		G721_ADPCM,
		MPEG,
		Experimental;
		
		/**
		 * Obtains the WaveFormat from the ID in the file.
		 * @param id The ID in the file.
		 * @return The WaveFormat, defaulting to Unknown.
		 */
		
		public static WaveFormat fromFormatID(int id) {
			
			switch (id) {
				case 1:
					return WaveFormat.PCM;
				case 2:
					return WaveFormat.ADPCM;
				case 6:
					return WaveFormat.G711_ALaw;
				case 7:
					return WaveFormat.G711_MuLaw;
				case 17:
					return WaveFormat.IMA_ADPCM;
				case 20:
					return WaveFormat.G723_ADPCM;
				case 49:
					return WaveFormat.GSM;
				case 64:
					return WaveFormat.G721_ADPCM;
				case 80:
					return WaveFormat.MPEG;
				case 65536:
					return WaveFormat.Experimental;
				default:
					return WaveFormat.Unknown;	
			}
			
		}
		
		/**
		 * Obtains the ID number of this format.
		 * @return The ID number to put in the file.
		 */
		
		public int getID() {
		
			switch (this) {
				case PCM:
					return 1;
				case ADPCM:
					return 2;
				case G711_ALaw:
					return 6;
				case G711_MuLaw:
					return 7;
				case IMA_ADPCM:
					return 17;
				case G723_ADPCM:
					return 20;
				case GSM:
					return 49;
				case G721_ADPCM:
					return 64;
				case MPEG:
					return 80;
				case Experimental:
					return 65536;
				default:
					return 0;
			}
			
		}
		
		/**
		 * Returns the description of the format.
		 * @return The description.
		 */
		
		public String getDescription() {
		
			switch (this) {
				case PCM:
					return "PCM (Uncompressed)";
				case ADPCM:
					return "Microsoft ADPCM";
				case G711_ALaw:
					return "G.711 (a-law)";
				case G711_MuLaw:
					return "G.711 (mu-law)";
				case IMA_ADPCM:
					return "IMA ADPCM";
				case G723_ADPCM:
					return "G.723 ADPCM";
				case GSM:
					return "GSM";
				case G721_ADPCM:
					return "G.721 ADPCM";
				case MPEG:
					return "MPEG Compressed";
				case Experimental:
					return "Experimental Codec";
				default:
					return "Unknown";
			}
			
		}
		
	}
	
	private WaveFormat format = WaveFormat.Unknown;
	private int channels = 0;
	private int sample_rate = 0;
	private int bits_per_sample = 0;
	
	private static final int MINIMUM_CHUNK_LENGTH = 16;
	private static final int INDEX_FORMAT = 0;
	private static final int LENGTH_FORMAT = 2;
	private static final int INDEX_CHANNELS = 2;
	private static final int LENGTH_CHANNELS = 2;
	private static final int INDEX_SAMPLE_RATE = 4;
	private static final int LENGTH_SAMPLE_RATE = 4;
	private static final int INDEX_BYTE_RATE = 8;
	private static final int LENGTH_BYTE_RATE = 4;
	private static final int INDEX_BLOCK_ALIGN = 12;
	private static final int LENGTH_BLOCK_ALIGN = 2;
	private static final int INDEX_BITS_PER_SAMPLE = 14;
	private static final int LENGTH_BITS_PER_SAMPLE = 2;
	private static final String CHNUK_PREFIX_FORMAT = "fmt ";
	private static Logger logger = Logger.getLogger(FormatChunk.class.getName());
	
	/**
	 * Generates a format chunk from the byte array of a file.
	 * @param data The data for this chunk.
	 * @return The format chunk if we could parse it. Otherwise NULL.
	 */
	
	public static FormatChunk fromBytes(byte[] data) {
		
		// Sanity check
		
		if (data == null || data.length < MINIMUM_CHUNK_LENGTH) {
			logger.log(Level.SEVERE, "Failed to parse the format chunk as it's not of the right length.");
			return null;
		}
		
		// Start the parsing process
		
		FormatChunk chunk = new FormatChunk();
		
		chunk.format = WaveFormat.fromFormatID(ByteUtil.byteToShortLittleEndian(Arrays.copyOfRange(data, INDEX_FORMAT, INDEX_FORMAT + LENGTH_FORMAT)));
		chunk.channels = ByteUtil.byteToShortLittleEndian(Arrays.copyOfRange(data, INDEX_CHANNELS, INDEX_CHANNELS + LENGTH_CHANNELS));
		chunk.sample_rate = ByteUtil.byteToIntLittleEndian(Arrays.copyOfRange(data, INDEX_SAMPLE_RATE, INDEX_SAMPLE_RATE + LENGTH_SAMPLE_RATE));
		chunk.bits_per_sample = ByteUtil.byteToShortLittleEndian(Arrays.copyOfRange(data, INDEX_BITS_PER_SAMPLE, INDEX_BITS_PER_SAMPLE + LENGTH_BITS_PER_SAMPLE));
		
		return chunk;
		
	}

	/* (non-Javadoc)
	 * @see uk.co.dlineradio.wavefilefixer.WAVEChunk#toBytes()
	 */
	@Override
	public byte[] toBytes() {
		
		ByteBuffer buffer = ByteBuffer.allocate(HEADER_TOTAL_LENGTH + MINIMUM_CHUNK_LENGTH);
		
		// Header
		
		buffer.put(ByteUtil.stringToByteArrayBigEndian(CHNUK_PREFIX_FORMAT));
		buffer.put(ByteUtil.intToByteArrayLittleEndian(MINIMUM_CHUNK_LENGTH));
		
		// Content
		
		int byte_rate = this.sample_rate * this.channels * this.bits_per_sample / 8;
		short block_align = (short) (this.channels * this.bits_per_sample / 8);
		
		buffer.put(ByteUtil.shortToByteArrayLittleEndian((short) this.format.getID()));
		buffer.put(ByteUtil.shortToByteArrayLittleEndian((short) this.channels));
		buffer.put(ByteUtil.intToByteArrayLittleEndian(this.sample_rate));
		buffer.put(ByteUtil.intToByteArrayLittleEndian(byte_rate));
		buffer.put(ByteUtil.shortToByteArrayLittleEndian(block_align));
		buffer.put(ByteUtil.shortToByteArrayLittleEndian((short) this.bits_per_sample));
		
		return buffer.array();
		
	}

	@Override
	public String toString() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("Format Chunk\n");
		sb.append(String.format("Format: %1$s\n", this.format.getDescription()));
		sb.append(String.format("Channels: %1$d\n", this.channels));
		sb.append(String.format("Sample Rate: %1$d\n", this.sample_rate));
		sb.append(String.format("Bits Per Sample: %1$d", this.bits_per_sample));
		sb.append("\n");
		
		return sb.toString();
		
	}
	
	/**
	 * Obtains the format of the data in the WAVE file.
	 * @return The format.
	 */

	public WaveFormat getFormat() {
		return format;
	}
	
	/**
	 * Sets the format of the data in the WAVE file.
	 * @param format The format.
	 */

	public void setFormat(WaveFormat format) {
		this.format = format;
	}
	
	/**
	 * Obtains the number of the channels in the WAVE file (e.g. 2 for stereo).
	 * @return The number of channels.
	 */

	public int getChannels() {
		return channels;
	}
	
	/**
	 * Sets the number of channels in the WAVE file (e.g. 2 for stereo).
	 * @param channels The number of channels.
	 */

	public void setChannels(int channels) {
		this.channels = channels;
	}
	
	/**
	 * Obtains the sample rate of the WAVE file.
	 * @return The sample rate.
	 */

	public int getSample_rate() {
		return sample_rate;
	}

	/**
	 * Sets the sample rate of the WAVE file.
	 * @param sample_rate The sample rate.
	 */
	
	public void setSample_rate(int sample_rate) {
		this.sample_rate = sample_rate;
	}
	
	/**
	 * Obtains the number of bits used in each sample.
	 * @return The number of bits.
	 */

	public int getBits_per_sample() {
		return bits_per_sample;
	}
	
	/**
	 * Sets the number of bits used in each sample.
	 * @param bits_per_sample The number of bits.
	 */

	public void setBits_per_sample(int bits_per_sample) {
		this.bits_per_sample = bits_per_sample;
	}
	
}
