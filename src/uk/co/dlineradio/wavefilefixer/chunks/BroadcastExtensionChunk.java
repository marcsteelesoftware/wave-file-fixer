package uk.co.dlineradio.wavefilefixer.chunks;

import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import uk.co.dlineradio.wavefilefixer.WAVEChunk;
import uk.co.dlineradio.wavefilefixer.util.ByteUtil;

/**
 * Broadcast extension chunk in a WAVE file.
 * @author Marc Steele
 */

public class BroadcastExtensionChunk extends WAVEChunk {
	
	private String description;
	private String originator;
	private String originatorReference;
	private Date originationDate;
	private long timeReference;
	private short version;
	private byte[] smtpeUMID;
	private String codingHistory;
	
	private static Logger logger = Logger.getLogger(BroadcastExtensionChunk.class.getName());
	private static SimpleDateFormat bextChunkDateFormat = new SimpleDateFormat("yyyy/MM/ddHH:mm:ss");
	private static SimpleDateFormat displayDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	private static final String CHUNK_PREFIX_BEXT = "bext";
	private static final short DEFAULT_VERSION = 1;
	private static final int MINIMUM_CHUNK_LENGTH = 602;
	private static final int INDEX_DESCRIPTION = 0;
	private static final int LENGTH_DESCRIPTION = 256;
	private static final int INDEX_ORIGINATOR = 256;
	private static final int LENGTH_ORIGINATOR = 32;
	private static final int INDEX_ORIGINATOR_REFERENCE = 288;
	private static final int LENGTH_ORIGINATOR_REFERENCE = 32;
	private static final int INDEX_ORIGINATION_DATE_TIME = 320;
	private static final int LENGTH_ORIGINATION_DATE_TIME = 18;
	private static final int INDEX_TIME_REFERNECE = 338;
	private static final int LENGTH_TIME_REFERENCE = 8;
	private static final int INDEX_VERSION = 346;
	private static final int LENGTH_VERSION = 2;
	private static final int INDEX_UMID = 348;
	private static final int LENGTH_UMID = 64;
	private static final int INDEX_RESERVED = 412;
	private static final int LENGTH_RESERVED = 190;
	private static final int INDEX_CODING_HISTORY = 602;
	private static final int LENGTH_TERMINATOR = 3;
	
	/**
	 * Generates a cart chunk from the byte array of a file.
	 * @param data The data for this chunk.
	 * @return The cart chunk if we could parse it. Otherwise NULL.
	 */
	
	public static BroadcastExtensionChunk fromBytes(byte[] data) {
		
		// Sanity check
		
		if (data == null || data.length < MINIMUM_CHUNK_LENGTH) {
			logger.log(Level.SEVERE, "Failed to parse the BEXT chunk as it's not of the right length.");
			return null;
		}
		
		// Inflate the chunk
		// Start with the basics
		
		BroadcastExtensionChunk chunk = new BroadcastExtensionChunk();
		
		chunk.description = ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, INDEX_DESCRIPTION, INDEX_DESCRIPTION + LENGTH_DESCRIPTION), true);
		chunk.originator = ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, INDEX_ORIGINATOR, INDEX_ORIGINATOR + LENGTH_ORIGINATOR), true);
		chunk.originatorReference = ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, INDEX_ORIGINATOR_REFERENCE, INDEX_ORIGINATOR_REFERENCE + LENGTH_ORIGINATOR_REFERENCE), true);
		chunk.timeReference = ByteUtil.byteToLongLittleEndian(Arrays.copyOfRange(data, INDEX_TIME_REFERNECE, INDEX_TIME_REFERNECE + LENGTH_TIME_REFERENCE));
		chunk.version = ByteUtil.byteToShortLittleEndian(Arrays.copyOfRange(data, INDEX_VERSION, INDEX_VERSION + LENGTH_VERSION));
		chunk.smtpeUMID = Arrays.copyOfRange(data, INDEX_UMID, INDEX_UMID + LENGTH_UMID);
		
		// Date / time
		
		String originationDateText = ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, INDEX_ORIGINATION_DATE_TIME, INDEX_ORIGINATION_DATE_TIME + LENGTH_ORIGINATION_DATE_TIME), true);
		
		try {
			
			chunk.originationDate = bextChunkDateFormat.parse(originationDateText);
			
		} catch (Exception ex) {
			logger.log(Level.WARNING, "Ran into a problem parsing the origination date. Falling back to now!", ex);
			chunk.originationDate = new Date();
		}
		
		// Optional coding history
		
		if (data.length > MINIMUM_CHUNK_LENGTH) {
			chunk.codingHistory = ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, INDEX_CODING_HISTORY, data.length - 1), true);
		}
		
		return chunk;
		
	}
	
	@Override
	public byte[] toBytes() {
		
		// Work out the length and generate a buffer
		
		int length = MINIMUM_CHUNK_LENGTH;
		if (this.codingHistory != null && !this.codingHistory.isEmpty()) {
			length += this.codingHistory.length() + LENGTH_TERMINATOR;
		}
		
		ByteBuffer buffer = ByteBuffer.allocate(HEADER_TOTAL_LENGTH + length);
		
		// Header
		
		buffer.put(ByteUtil.stringToByteArrayBigEndian(CHUNK_PREFIX_BEXT));
		buffer.put(ByteUtil.intToByteArrayLittleEndian(length));
		
		// And now the data
		
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.stringToByteArrayBigEndian(this.description), LENGTH_DESCRIPTION));
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.stringToByteArrayBigEndian(this.originator), LENGTH_ORIGINATOR));
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.stringToByteArrayBigEndian(this.originatorReference), LENGTH_ORIGINATOR_REFERENCE));
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.stringToByteArrayBigEndian(bextChunkDateFormat.format(this.originationDate)), LENGTH_ORIGINATION_DATE_TIME));
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.longToByteArrayLittleEndian(this.timeReference), LENGTH_TIME_REFERENCE));
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.shortToByteArrayLittleEndian(DEFAULT_VERSION), LENGTH_VERSION));
		buffer.put(ByteUtil.safeCopyOf(this.smtpeUMID, LENGTH_UMID));
		
		byte[] reservedData = new byte[LENGTH_RESERVED];
		Arrays.fill(reservedData, (byte) 0);
		buffer.put(reservedData);
		
		if (this.codingHistory != null && !this.codingHistory.isEmpty()) {
			buffer.put(ByteUtil.safeCopyOf(ByteUtil.stringToByteArrayBigEndian(this.codingHistory), this.codingHistory.length()));
			buffer.put(ByteUtil.stringToByteArrayBigEndian("\r\n"));
			buffer.put((byte) 0);
		}
		
		return buffer.array();
		
	}

	@Override
	public String toString() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("Broacast Extension Chunk\n");
		
		sb.append(String.format("Description: %1$s\n", this.description));
		sb.append(String.format("Originator: %1$s\n", this.originator));
		sb.append(String.format("Originator Reference: %1$s\n", this.originatorReference));
		sb.append(String.format("Origination Date: %1$s\n", displayDateFormat.format(this.originationDate)));
		sb.append(String.format("Origination Time: %1$d samples since midnight\n", this.timeReference));
		sb.append(String.format("Broadcast Extension Version: %1$d\n", this.version));
		sb.append(String.format("Unique Media ID: %1$s\n", Arrays.toString(this.smtpeUMID)));
		
		if (this.codingHistory != null && !this.codingHistory.isEmpty()) {
			sb.append(String.format("Coding History: %1$s\n", this.codingHistory));
		}
		
		sb.append("\n");
		
		return sb.toString();
		
	}
	
	/**
	 * Obtains the description of the audio.
	 * @return The description of the audio.
	 */

	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description of the audio.
	 * @param description The description of the audio.
	 */

	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Obtains the originator of the audio.
	 * @return The originator of the audio.
	 */

	public String getOriginator() {
		return originator;
	}
	
	/**
	 * Sets the originator of the audio.
	 * @param originator The originator.
	 */

	public void setOriginator(String originator) {
		this.originator = originator;
	}
	
	/**
	 * Obtains the originator's reference for the audio.
	 * @return The originator's reference for the audio.
	 */

	public String getOriginatorReference() {
		return originatorReference;
	}
	
	/**
	 * Sets the originator's reference for the audio.
	 * @param originatorReference The originator's reference for the audio.
	 */

	public void setOriginatorReference(String originatorReference) {
		this.originatorReference = originatorReference;
	}
	
	/**
	 * Obtains the date the audio was recorded.
	 * @return The date the audio was recorded.
	 */

	public Date getOriginationDate() {
		return originationDate;
	}
	
	/**
	 * Sets the date the audio was recorded.
	 * @param originationDate The date the audio was recorded.
	 */

	public void setOriginationDate(Date originationDate) {
		this.originationDate = originationDate;
	}
	
	/**
	 * Obtains the time the audio was recorded.
	 * @return The time in samples since midnight.
	 */

	public long getTimeReference() {
		return timeReference;
	}
	
	/**
	 * Sets the time the audio was recorded.
	 * @param timeReference The time in samples since midnight.
	 */

	public void setTimeReference(long timeReference) {
		this.timeReference = timeReference;
	}
	
	/**
	 * Obtains the BEXT chunk version.
	 * @return The BEXT chunk version.
	 */

	public short getVersion() {
		return version;
	}
	
	/**
	 * Sets the BEXT chunk version.
	 * @param version The BEXT chunk version.
	 */

	public void setVersion(short version) {
		this.version = version;
	}
	
	/**
	 * Obtains the unique media ID of the cart.
	 * @return The unique media ID.
	 */

	public byte[] getSmtpeUMID() {
		return smtpeUMID;
	}
	
	/**
	 * Sets the unique media ID of the cart.
	 * @param smtpeUMID The unique media ID.
	 */

	public void setSmtpeUMID(byte[] smtpeUMID) {
		this.smtpeUMID = smtpeUMID;
	}
	
	/**
	 * Obtains the coding history of the cart.
	 * @return The coding history of the cart.
	 */

	public String getCodingHistory() {
		return codingHistory;
	}
	
	/**
	 * Sets the coding history of the cart.
	 * @param codingHistory The coding history.
	 */
	
	public void setCodingHistory(String codingHistory) {
		this.codingHistory = codingHistory;
	}

}
