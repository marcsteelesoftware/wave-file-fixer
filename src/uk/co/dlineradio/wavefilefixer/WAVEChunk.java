package uk.co.dlineradio.wavefilefixer;

/**
 * Base class for a WAVE chunk.
 * @author Marc Steele
 */

public abstract class WAVEChunk {
	
	public static final int HEADER_TOTAL_LENGTH  = 8;
	public static final int HEADER_ID_LENGTH = 4;
	public static final int HEADER_ID_INDEX = 0;
	public static final int HEADER_LENGTH_LENGTH = 4;
	public static final int HEADER_LENGTH_INDEX = 4;

	/**
	 * Converts the chunk to bytes for writing to a file.
	 * @return The byte array encoding of the chunk.
	 */
	
	public abstract byte[] toBytes();
	
	@Override
	public abstract String toString();

}
