package uk.co.dlineradio.wavefilefixer.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * File utility methods.
 * @author Marc Steele
 */

public class FileUtil {
	
	private static final int BUFFER_SIZE = 1024;
	private static Logger logger = Logger.getLogger(FileUtil.class.getName());
	
	/**
	 * Reads a complete file from disk.
	 * @param file_name The file to be read.
	 * @return The raw contents of the file if all is well. Otherwise, NULL.
	 */
	
	public static byte[] readCompleteFile(String file_name) {
		
		// Sanity check
		
		if (file_name == null) {
			logger.log(Level.SEVERE, "Asked to read in a complete file. However, we weren't supplied a file name so we couldn't do it!");
			return null;
		}
		
		File file = new File(file_name);
		if (!file.exists() || !file.canWrite()) {
			logger.log(Level.SEVERE, String.format("Asked to read in the file %1$s but it appears we either can't find it or can't access it.", file_name));
			return null;
		}
		
		// Generate the destination array
		
		long file_length = file.length();
		if (file_length < 0 || file_length > Integer.MAX_VALUE) {
			logger.log(Level.SEVERE, String.format("Could not read in file %1$s as a silly length was returned.", file_name));
			return null;
		}
		
		byte[] file_contents = new byte[(int) file_length];
		
		// Pull in the file a bit at a time
		
		try {
			
			FileInputStream file_in = new FileInputStream(file);
			byte[] buffer = new byte[BUFFER_SIZE];
			int read_length = file_in.read(buffer);
			int current_position = 0;
			
			while (read_length > 0) {
				
				// Append
				
				if (read_length + current_position > file_length) {
					logger.log(Level.SEVERE, String.format("Could not read rest of file as we ran off the end of the expected size.", file_name));
					file_in.close();
					return null;
				} else {
					
					for (int i = 0; i < read_length; i++) {
						file_contents[current_position + i] = buffer[i];
					}
					
				}
				
				// Read on
				
				current_position += read_length;
				read_length = file_in.read(buffer);
				
			}
			
			file_in.close();
			
		} catch (FileNotFoundException e) {
			logger.log(Level.SEVERE, String.format("Weird. It appears that the file %1$s disappeared after we checked it.", file_name), e);
			return null;
		} catch (IOException e) {
			logger.log(Level.SEVERE, String.format("We ran into an IO issue reading in the file %1$s", file_name), e);
			return null;
		}
		
		return file_contents;
		
	}

}
