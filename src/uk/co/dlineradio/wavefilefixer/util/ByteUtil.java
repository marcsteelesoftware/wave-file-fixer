package uk.co.dlineradio.wavefilefixer.util;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utility methods for doing things with bytes.
 * @author Marc Steele
 */

public class ByteUtil {
	
	private static final int LENGTH_INT = 4;
	private static final int LENGTH_SHORT = 2;
	private static final int LENGTH_LONG = 8;
	private static final String LITTLE_ENDIAN = "UTF-8";
	private static final String BIG_ENDIAN = "UTF-8";
	private static Charset ascii = Charset.forName("US-ASCII");
	private static Logger logger = Logger.getLogger(ByteUtil.class.getName());
	
	/**
	 * Converts a big endian byte array to an integer value.
	 * @param data The value to convert.
	 * @return The converted value.
	 */
	
	public static int byteToIntBigEndian(byte[] data) {
		
		// Sanity check
		
		if (data == null || data.length < LENGTH_INT) {
			return -1;
		}
		
		// Perform the conversion
		
		ByteBuffer buffer = ByteBuffer.allocate(LENGTH_INT);
		buffer.order(ByteOrder.BIG_ENDIAN).put(data);
		buffer.flip();
		return buffer.getInt();
		
	}
	
	/**
	 * Converts a little endian byte array to an integer value.
	 * @param data The value to convert.
	 * @return The converted value.
	 */
	
	public static int byteToIntLittleEndian(byte[] data) {
		
		// Sanity check
		
		if (data == null || data.length < LENGTH_INT) {
			return -1;
		}
				
		// Perform the conversion
				
		ByteBuffer buffer = ByteBuffer.allocate(LENGTH_INT);
		buffer.order(ByteOrder.LITTLE_ENDIAN).put(data);
		buffer.flip();
		return buffer.getInt();
		
	}
	
	/**
	 * Converts a little endian byte array to a short value.
	 * @param data The data we want to convert.
	 * @return The result. It's an int as we can't get an unsigned short.
	 */
	
	public static short byteToShortLittleEndian(byte[] data) {
		
		// Sanity check
		
		if (data == null || data.length < LENGTH_SHORT) {
			return -1;
		}
		
		// Perform the conversion
		
		ByteBuffer buffer = ByteBuffer.allocate(LENGTH_SHORT);
		buffer.order(ByteOrder.LITTLE_ENDIAN).put(data);
		buffer.flip();
		return buffer.getShort();
		
	}
	
	/**
	 * Converts a big endian byte array to a short value.
	 * @param data The data we want to convert.
	 * @return The result. It's an int as we can't get an unsigned short.
	 */
	
	public static short byteToShortBigEndian(byte[] data) {
		
		// Sanity check
		
		if (data == null || data.length < LENGTH_SHORT) {
			return -1;
		}
				
		// Perform the conversion
				
		ByteBuffer buffer = ByteBuffer.allocate(LENGTH_SHORT);
		buffer.order(ByteOrder.BIG_ENDIAN).put(data);
		buffer.flip();
		return buffer.getShort();
		
	}
	
	/**
	 * Converts a big endian byte array to a string. 
	 * @param data The data to convert.
	 * @param trim TRUE if the string should be trimmed. Otherwise FALSE.
	 * @return The converted value.
	 */
	
	public static String byteToStringBigEndian(byte[] data, boolean trim) {
		
		// Sanity check
		
		if (data == null || data.length == 0) {
			return null;
		}
		
		// Perform the conversion
		
		ByteBuffer buffer = ByteBuffer.allocate(data.length);
		buffer.order(ByteOrder.BIG_ENDIAN).put(data);
		buffer.flip();
		
		
		try {
			
			CharsetDecoder decoder = ascii.newDecoder();
			String text = decoder.decode(buffer).toString();
			if (text != null && trim) {
				text = text.trim();
			}
			
			return text;
			
		} catch (CharacterCodingException e) {
			logger.log(Level.SEVERE, "Ran into a character decoding issue converting bytes to String.", e);
			return null;
		}
		
		
	}
	
	/**
	 * Generates a byte array from a string.
	 * @param source The source string we will work from.
	 * @param byte_order The byte order of the string.
	 * @return The byte array if all went well. Otherwise NULL.
	 */
	
	private static byte[] stringToByteArray(String source, ByteOrder byte_order) {
		
		// Sanity check
		
		if (source == null || source.isEmpty()) {
			logger.log(Level.SEVERE, "Could not convert the string to a byte array as no string was provided.");
			return null;
		}
		
		if (byte_order == null) {
			logger.log(Level.SEVERE, "Could not convert the string to a byte array as no byte order was specified.");
			return null;
		}
		
		// Perform the conversion
		
		try {
		
			if (byte_order == ByteOrder.LITTLE_ENDIAN) {
				return source.getBytes(LITTLE_ENDIAN);
			} else {
				return source.getBytes(BIG_ENDIAN);
			}
		
		} catch (UnsupportedEncodingException e) {
			logger.log(Level.SEVERE, "Turns out we don't have support for little and big endian!");
			return null;
		}
		
	}
	
	/**
	 * Converts a string to a big endian byte array.
	 * @param source The string to convert.
	 * @return The converted string if all went well. Otherwise null.
	 */
	
	public static byte[] stringToByteArrayBigEndian(String source) {
		return stringToByteArray(source, ByteOrder.BIG_ENDIAN);
	}
	
	/**
	 * Converts a string to a little endian byte array.
	 * @param source The string to convert.
	 * @return The converted string if all went well. Otherwise null.
	 */
	
	public static byte[] stringToByteArrayLittleEndian(String source) {
		return stringToByteArray(source, ByteOrder.LITTLE_ENDIAN);
	}
	
	/**
	 * Converts an integer to a byte array.
	 * @param value The integer to convert.
	 * @param byte_order The byte order we want it in.
	 * @return The converted value.
	 */
	
	private static byte[] intToByteArray(int value, ByteOrder byte_order) {
		
		ByteBuffer dest = ByteBuffer.allocate(LENGTH_INT);
		dest.order(byte_order);
		dest.putInt(value);
		return dest.array();
		
	}
	
	/**
	 * Converts an integer to a byte array in big endian order.
	 * @param value The value to convert.
	 * @return The converted value.
	 */
	
	public static byte[] intToByteArrayBigEndian(int value) {
		return intToByteArray(value, ByteOrder.BIG_ENDIAN);
	}
	
	/**
	 * Converts an integer to a byte array in little endian order.
	 * @param value The value to convert.
	 * @return The converted value.
	 */
	
	public static byte[] intToByteArrayLittleEndian(int value) {
		return intToByteArray(value, ByteOrder.LITTLE_ENDIAN);
	}
	
	/**
	 * Converts a short to a byte array.
	 * @param value The value to convert.
	 * @param byte_order The byte order we want it in.
	 * @return The converted value.
	 */
	
	private static byte[] shortToByteArray(short value, ByteOrder byte_order) {
		
		ByteBuffer dest = ByteBuffer.allocate(LENGTH_SHORT);
		dest.order(byte_order);
		dest.putShort(value);
		return dest.array();
		
	}
	
	/**
	 * Converts a short to a byte array in big endian order.
	 * @param value The value to convert.
	 * @return The converted value.
	 */
	
	public static byte[] shortToByteArrayBigEndian(short value) {
		return shortToByteArray(value, ByteOrder.BIG_ENDIAN);
	}
	
	/**
	 * Converts a short to a byte array in little endian order.
	 * @param value The value to convert.
	 * @return The converted value.
	 */
	
	public static byte[] shortToByteArrayLittleEndian(short value) {
		return shortToByteArray(value, ByteOrder.LITTLE_ENDIAN);
	}
	
	/**
	 * Converts a long value into a byte array.
	 * @param value The value to convert.
	 * @param byteOrder The byte order we want it in.
	 * @return The converted value.
	 */
	
	private static byte[] longToByteArray(long value, ByteOrder byteOrder) {
		
		ByteBuffer dest = ByteBuffer.allocate(LENGTH_LONG);
		dest.order(byteOrder);
		dest.putLong(value);
		return dest.array();
		
	}
	
	/**
	 * Converts a long to a byte array in big endian order.
	 * @param value The value to convert.
	 * @return The converted value.
	 */
	
	public static byte[] longToByteArrayBigEndian(long value) {
		return longToByteArray(value, ByteOrder.BIG_ENDIAN);
	}
	
	/**
	 * Converts a long to a byte array in little endian order.
	 * @param value The value to convert.
	 * @return The converted value.
	 */
	
	public static byte[] longToByteArrayLittleEndian(long value) {
		return longToByteArray(value, ByteOrder.LITTLE_ENDIAN);
	}
	
	/**
	 * Convers a byte array to a long value.
	 * @param data The byte array we're interested in.
	 * @param byteOrder The byte order we've got the data in.
	 * @return The converted value. Note that we will supply 0 for duff values.
	 */
	
	private static long byteToLong(byte[] data, ByteOrder byteOrder) {
		
		// Sanity check
		
		if (data == null || data.length < LENGTH_LONG) {
			logger.log(Level.WARNING, "Asked to convert a byte array to a long value. As no data was supplied, we're bailing out.");
			return 0;
		}
		
		// Perform the conversion
		
		ByteBuffer buffer = ByteBuffer.allocate(LENGTH_LONG);
		buffer.order(byteOrder).put(data);
		buffer.flip();
		return buffer.getLong();
		
	}
	
	/**
	 * Converts a byte array to a long value (little endian).
	 * @param data The data to convert.
	 * @return The converted value.
	 */
	
	public static long byteToLongLittleEndian(byte[] data) {
		return byteToLong(data, ByteOrder.LITTLE_ENDIAN);
	}
	
	/**
	 * Converts a byte array to a long value (big endian).
	 * @param data The data to convert.
	 * @return The converted value.
	 */
	
	public static long byteToLongBigEndian(byte[] data) {
		return byteToLong(data, ByteOrder.BIG_ENDIAN);
	}
	
	/**
	 * Performs a safe version of the Arrays.copyOf routine.
	 * @param data The data we'll copying.
	 * @param length The length we want the destination array to be.
	 * @return The converted array.
	 */
	
	public static byte[] safeCopyOf(byte[] data, int length) {
		
		if (length <= 0) {
			return null;
		} else if (data == null || data.length == 0) {
			
			byte[] blank = new byte[length];
			Arrays.fill(blank, (byte) 0);
			return blank;
			
		} else {
			return Arrays.copyOf(data, length);
		}
		
	}
	
}
