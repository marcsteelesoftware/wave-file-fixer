package uk.co.dlineradio.wavefilefixer;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import uk.co.dlineradio.wavefilefixer.util.ByteUtil;
import uk.co.dlineradio.wavefilefixer.util.FileUtil;

/**
 * An object representation of a WAVE file. Can also be used to generate
 * a valid file.
 * @author Marc Steele
 */

public class WAVEFile {

	private Map<String, WAVEChunk> chunks = new HashMap<String, WAVEChunk>();
	
	private static final byte[] HEADER_RIFF_CONTENT = { 0x52, 0x49, 0x46, 0x46 };
	private static final byte[] HEADER_WAVE_CONTENT = { 0x57, 0x41, 0x56, 0x45 };
	private static final String[] OUTPUT_CHUNK_ORDER = { "fmt ", "bext", "cart", "data" };
	private static final int HEADER_RIFF_LENGTH = 4;
	private static final int HEADER_RIFF_OFFSET = 0;
	private static final int HEADER_WAVE_LENGTH  = 4;
	private static final int HEADER_WAVE_OFFSET = 8;
	private static final int LENGTH_FILE_HEADER = 12;
	private static final int CHUNK_HEADER_LENGTH = 8;
	private static final int CHUNK_TYPE_LENGTH = 4;
	private static final int CHUNK_TYPE_OFFSET = 0;
	private static final int CHUNK_LENGTH_LENGTH = 4;
	private static final int CHUNK_LENGTH_OFFSET = 4;
	private static final int SEARCH_MAX_OFFSET = 5;
	private static final int LENGTH_TOTAL_LENGTH = 4;
	
	private static Map<String, String> chunk_map = new HashMap<String, String>();
	private static Logger logger = Logger.getLogger(WAVEFile.class.getName());
	
	static {
		
		// Generate the mapping to the chunk classes
		
		chunk_map.put("fmt ", "uk.co.dlineradio.wavefilefixer.chunks.FormatChunk");
		chunk_map.put("bext", "uk.co.dlineradio.wavefilefixer.chunks.BroadcastExtensionChunk");
		chunk_map.put("cart", "uk.co.dlineradio.wavefilefixer.chunks.CartChunk");
		chunk_map.put("data", "uk.co.dlineradio.wavefilefixer.chunks.DataChunk");
		
	}
	
	/**
	 * We generate new instances through a static interface.
	 */
	
	private WAVEFile() { }
	
	/**
	 * Generates a WAVE file object from a file on the disk.
	 * @param file_name The file we want.
	 * @return The file inflated if all went well. Otherwise NULL.
	 */
	
	public static WAVEFile fromFile(String file_name) {
		
		// Attempt to read in the file from disk
		
		byte[] file_contents = FileUtil.readCompleteFile(file_name);
		if (file_contents == null) {
			logger.log(Level.SEVERE, String.format("Could not inflate WAVE file %1$s as we couldn't read its contents.", file_name));
			return null;
		} else {
			logger.log(Level.INFO, String.format("Successfully read in file %1$s with a length of %2$d bytes.", file_name, file_contents.length));
		}
		
		// Check it's a valid WAVE file
		
		if (file_contents.length < LENGTH_FILE_HEADER) {
			logger.log(Level.SEVERE, String.format("%1$s cannot be a valid WAVE file as it's smaller than the required header!", file_name));
			return null;
		}
		
		if (!Arrays.equals(HEADER_RIFF_CONTENT, Arrays.copyOfRange(file_contents, HEADER_RIFF_OFFSET, HEADER_RIFF_OFFSET + HEADER_RIFF_LENGTH))) {
			logger.log(Level.SEVERE, String.format("%1$s cannot be a valid WAVE file as it's missing the RIFF header.", file_name));
			return null;
		}
		
		if (!Arrays.equals(HEADER_WAVE_CONTENT, Arrays.copyOfRange(file_contents, HEADER_WAVE_OFFSET, HEADER_WAVE_OFFSET + HEADER_WAVE_LENGTH))) {
			logger.log(Level.SEVERE, String.format("%1$s cannot be a valid WAVE file as it's missing the WAVE header.", file_name));
			return null;
		}
		
		logger.log(Level.INFO, String.format("Looks like %1$s is a valid RIFF/WAVE file. We'll now go on to parse the chunks.", file_name));
		
		// Now read it a chunk at a time
		
		WAVEFile wave_file = new WAVEFile();
		int current_position = LENGTH_FILE_HEADER;
		
		while (current_position + CHUNK_HEADER_LENGTH < file_contents.length) {
			
			// Read off the chunk details
			
			String chunk_type = readStringFromDataBigEndian(file_contents, current_position + CHUNK_TYPE_OFFSET, CHUNK_TYPE_LENGTH);
			int chunk_length = readIntFromDataLittleEndian(file_contents, current_position + CHUNK_LENGTH_OFFSET, CHUNK_LENGTH_LENGTH);
			
			// Check we're valid
			
			boolean chunk_valid = isChunkTypeValid(chunk_type);
			
			if (!chunk_valid) {
				
				// We'll search forward through the file a bit 
				
				int current_offset = 1;
				while (current_offset <= SEARCH_MAX_OFFSET && !chunk_valid) {
					
					chunk_type = readStringFromDataBigEndian(file_contents, current_position + current_offset + CHUNK_TYPE_OFFSET, CHUNK_TYPE_LENGTH);
					chunk_length = readIntFromDataLittleEndian(file_contents, current_position + current_offset + CHUNK_LENGTH_OFFSET, CHUNK_LENGTH_LENGTH);
					
					chunk_valid = isChunkTypeValid(chunk_type);
					if (!chunk_valid) {
						current_offset++;
					}
					
				}
				
				// Have we found the valid chunk yet
				
				if (chunk_valid) {
					current_position += current_offset;
				} else {
					
					// Let's try looking backwards
					
					current_offset = -1;
					while (Math.abs(current_offset) <= SEARCH_MAX_OFFSET && !chunk_valid) {
						
						chunk_type = readStringFromDataBigEndian(file_contents, current_position + current_offset + CHUNK_TYPE_OFFSET, CHUNK_TYPE_LENGTH);
						chunk_length = readIntFromDataLittleEndian(file_contents, current_position + current_offset + CHUNK_LENGTH_OFFSET, CHUNK_LENGTH_LENGTH);
						
						chunk_valid = isChunkTypeValid(chunk_type);
						if (!chunk_valid) {
							current_offset--;
						}
						
					}
					
					// Last chance saloon
					
					if (!chunk_valid) {
						logger.log(Level.SEVERE, String.format("Bailing out checking the file %1$s as we couldn't find valid chunks.", file_name));
						return null;
					}
					
				}
				
			}
			
			// We've found the chunk - let's read in those glorious bytes!
			
			logger.log(Level.INFO, String.format("Successfully detected %1$s chunk of length %2$d bytes.", chunk_type, chunk_length));
			byte[] chunk_data = Arrays.copyOfRange(file_contents, current_position + CHUNK_HEADER_LENGTH, Math.min(file_contents.length - 1, current_position + CHUNK_HEADER_LENGTH + chunk_length));
			
			// Let's find the correct chunk type and instantiate it
			
			String chunk_class_name = chunk_map.get(chunk_type.toLowerCase());
			if (chunk_class_name == null) {
				logger.log(Level.WARNING, String.format("Looks like %1$s is not a chunk type we know about.", chunk_type));
			} else {
				
				try {
					
					Class chunk_class = Class.forName(chunk_class_name);
					Method instantiate_method = chunk_class.getDeclaredMethod("fromBytes", new Class[] {byte[].class});
					WAVEChunk inflated_chunk = (WAVEChunk) instantiate_method.invoke(null, chunk_data);
					
					if (inflated_chunk != null) {
						wave_file.chunks.put(chunk_type.toLowerCase(), inflated_chunk);
						logger.log(Level.INFO, String.format("Successfully inflated chunk:\n\n%1$s", inflated_chunk));
					}
					
				} catch (ClassNotFoundException e) {
					logger.log(Level.SEVERE, String.format("Turns out we couldn't find the chunk class inflating a %1$s chunk.", chunk_type), e);
				} catch (NoSuchMethodException e) {
					logger.log(Level.SEVERE, String.format("The methods we need to inflate a %1$s chunk don't exist.", chunk_type), e);
				} catch (SecurityException e) {
					logger.log(Level.SEVERE, String.format("Security problems say we're not allowed to inflate a %1$s chunk.", chunk_type), e);
				} catch (IllegalAccessException e) {
					logger.log(Level.SEVERE, String.format("Illegal access issues occured trying to inflate a %1$s chunk.", chunk_type), e);;
				} catch (IllegalArgumentException e) {
					logger.log(Level.SEVERE, String.format("We supplied the wrong variables trying to inflate a %1$s chunk.", chunk_type), e);
				} catch (InvocationTargetException e) {
					logger.log(Level.SEVERE, String.format("The method we call to inflate a %1$s chunk claims to not be invokable.", chunk_type), e);
				} 
				
			}
			
			// Continue onwards
			
			current_position += CHUNK_HEADER_LENGTH + chunk_length;
			
		}
		
		return wave_file;
		
	}
	
	/**
	 * Checks a chunk type to see if it's valid.
	 * @param type The type name we're checking.
	 * @return TRUE if it's valid. Otherwise FALSE.
	 */
	
	private static boolean isChunkTypeValid(String type) {
		
		// Sanity check
		
		if (type == null) {
			return false;
		}
		
		// Do a regex match for the first character being A-Z
		// Oh, and it's of the right length :P
		
		return type.matches("[A-Za-z]+.?") && type.length() == CHUNK_TYPE_LENGTH;
		
	}
	
	/**
	 * Reads a string in from a specific part of the data.
	 * @param data The bytes we'll be pulling from.
	 * @param start The start byte.
	 * @param length The number of bytes we'll read.
	 * @return The data as a string value.
	 */
	
	private static String readStringFromDataBigEndian(byte[] data, int start, int length) {
		return ByteUtil.byteToStringBigEndian(Arrays.copyOfRange(data, Math.max(start, 0), Math.min(start + length, data.length - 1)), false);
	}
	
	/**
	 * Reads in an integer from a specific part of the data.
	 * @param data The bytes we'll be pulling from.
	 * @param start The start byte.
	 * @param length The number of bytes we'll read.
	 * @return The data as an integer value
	 */
	
	private static int readIntFromDataLittleEndian(byte[] data, int start, int length) {
		return ByteUtil.byteToIntLittleEndian(Arrays.copyOfRange(data, Math.max(start, 0), Math.min(start + length, data.length - 1)));
	}
	
	/**
	 * Converts the WAVE file to a byte array (for writing to disk).
	 * @return The byte representation of this file.
	 */
	
	public byte[] toBytes() {
		
		// Determine how much space we need by converting each of the sub chunks
		
		long length = 0;
		
		for (String currentChunkType:OUTPUT_CHUNK_ORDER) {
			
			WAVEChunk currentChunk = this.chunks.get(currentChunkType);
			if (currentChunk != null) {
				
				byte[] data = currentChunk.toBytes();
				if (data != null) {
					length += data.length;
				}
				
			}
			
		}
		
		// Now generate the buffer we'll write into
		
		ByteBuffer buffer = ByteBuffer.allocate((int) (LENGTH_FILE_HEADER + length));
		
		// Fill it - header then contents
		
		buffer.put(HEADER_RIFF_CONTENT);
		buffer.put(ByteUtil.safeCopyOf(ByteUtil.intToByteArrayLittleEndian((int) length), LENGTH_TOTAL_LENGTH));
		buffer.put(HEADER_WAVE_CONTENT);
		
		for (String currentChunkType:OUTPUT_CHUNK_ORDER) {
			
			WAVEChunk currentChunk = this.chunks.get(currentChunkType);
			if (currentChunk != null) {
				buffer.put(currentChunk.toBytes());				
			}
			
		}
		
		return buffer.array();
		
	}
	
	/**
	 * Obtains the chunks this file is made up of.
	 * @return The chunks.
	 */

	public Map<String, WAVEChunk> getChunks() {
		return chunks;
	}

}
