package uk.co.dlineradio.wavefilefixer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import uk.co.dlineradio.wavefilefixer.chunks.BroadcastExtensionChunk;
import uk.co.dlineradio.wavefilefixer.chunks.CartChunk;
import uk.co.dlineradio.wavefilefixer.chunks.CartTimer;
import uk.co.dlineradio.wavefilefixer.chunks.DataChunk;
import uk.co.dlineradio.wavefilefixer.chunks.FormatChunk;

/**
 * Launcher for the application.
 * @author Marc Steele
 */

public class Launcher {
	
	private static Logger logger = Logger.getLogger(Launcher.class.getName());

	/**
	 * Launches the application.
	 * @param The command line arguments.
	 */
	
	public static void main(String[] args) {
		
		// Pull in and check the file names from the command line arguments
		
		List<String> valid_file_names = new LinkedList<String>();
		String destination_directory = null;
		
		for (String current_file_name:args) {
			
			File current_file = new File(current_file_name);
			if (current_file.exists() && current_file.canRead()) {
				
				// If we get a directory, it's where we're punting to.
				
				if (current_file.isDirectory()) {
					destination_directory = current_file_name;
				} else {
					valid_file_names.add(current_file_name);
				}
				
			}
			
		}
		
		// Sanity check
		
		if (valid_file_names.isEmpty()) {
			logger.log(Level.SEVERE, "As no file names were supplied, we couldn't check any files.");
			return;
		}
		
		if (destination_directory == null) {
			logger.log(Level.SEVERE, "As there was no destination directory supplied, we couldn't process the files.");
			return;
		}
		
		// Process the files
		
		for (String current_file_name:valid_file_names) {
			
			// Generate the WAVE object
			
			logger.log(Level.INFO, String.format("Starting to process %1$s", current_file_name));
			WAVEFile current_wave_file = WAVEFile.fromFile(current_file_name);
			
			if (current_wave_file == null) {
				logger.log(Level.WARNING, String.format("Aborting processing of %1$s", current_file_name));
				continue;
			}
			
			// Check we have the required chunks
			
			BroadcastExtensionChunk bext_chunk = (BroadcastExtensionChunk) current_wave_file.getChunks().get("bext");
			CartChunk cart_chunk = (CartChunk) current_wave_file.getChunks().get("cart");
			FormatChunk format_chunk = (FormatChunk) current_wave_file.getChunks().get("fmt ");
			DataChunk data_chunk = (DataChunk) current_wave_file.getChunks().get("data");
			
			if (bext_chunk == null) {
				logger.log(Level.WARNING, String.format("Could not continue processing the %1$s file as it's missing the Broadcast Extension chunk.", current_file_name));
				continue;
			} else if (cart_chunk == null) {
				logger.log(Level.WARNING, String.format("Could not continue processing the %1$s file as it's missing the Cart chunk.", current_file_name));
				continue;
			} else if (format_chunk == null) {
				logger.log(Level.WARNING, String.format("Could not continue processing the %1$s file as it's missing the Format chunk.", current_file_name));
				continue;
			} else if (data_chunk == null) {
				logger.log(Level.WARNING, String.format("Could not continue processing the %1$s file as it's missing the Data chunk.", current_file_name));
				continue;
			}
			
			// Fix any timers
			
			if (cart_chunk.getTimers().get("SEGs") == null && cart_chunk.getTimers().get("SEGe") == null) {
				
				CartTimer secondary_start = cart_chunk.getTimers().get("SEC1");
				CartTimer secondary_end = cart_chunk.getTimers().get("SEC2");
				CartTimer audio_end = cart_chunk.getTimers().get("AUDe");
				
				if (secondary_start != null && secondary_end != null && audio_end != null) {
					
					secondary_start.setType("SEGs");
					secondary_end.setType("SEGe");
					secondary_end.setValue(audio_end.getValue());
					logger.log(Level.INFO, "Spotted that the cart chunk timers were configured to use the secondary values. Now changed to use the segue values.");
					
				}
				
			}
			
			if (cart_chunk.getTimers().get("INT") != null && cart_chunk.getTimers().get("INTs") == null && cart_chunk.getTimers().get("AUDs") != null) {
				
				CartTimer old_intro = cart_chunk.getTimers().get("INT");
				CartTimer audio_start = cart_chunk.getTimers().get("AUDs");
				CartTimer intro_start = new CartTimer();
				CartTimer intro_end = new CartTimer();
				
				intro_start.setType("INTs");
				intro_end.setType("INTe");
				intro_start.setValue(audio_start.getValue());
				intro_end.setValue(old_intro.getValue());
				
				cart_chunk.getTimers().remove("INT");
				cart_chunk.getTimers().put(intro_start.getType(), intro_start);
				cart_chunk.getTimers().put(intro_end.getType(), intro_end);
				
				logger.log(Level.INFO, "Spotted that the intro timers weren't quite configured correctly... fixed now!");
				
			}
			
			// Check we have an artist and title
			
			if (cart_chunk.getArtist() == null || cart_chunk.getArtist().isEmpty()) {
				logger.log(Level.WARNING, String.format("Could not determine the artist for %1$s. We'll stop trying to fix it.", current_file_name));
				continue;
			}
			
			if (cart_chunk.getTitle() == null || cart_chunk.getTitle().isEmpty()) {
				logger.log(Level.WARNING, String.format("Could not determine the title for %1$s. We'll stop trying to fix it.", current_file_name));
				continue;
			}
			
			logger.log(Level.INFO, String.format("Determined that the cart is %1$s by %2$s", cart_chunk.getTitle(), cart_chunk.getArtist()));
			
			// Punt the complete file out to disk
			
			String output_file_name = String.format("%1$s_%2$s.wav", cart_chunk.getArtist(), cart_chunk.getTitle());
			output_file_name = output_file_name.replaceAll("[/`?*\\<>|:;]", "");
			
			try {
				
				File output_file = new File(destination_directory, output_file_name);
				FileOutputStream output_stream = new FileOutputStream(output_file);
				output_stream.write(current_wave_file.toBytes());
				output_stream.close();
				
				logger.info(String.format("Successfully written new file to %1$s.", output_file.getAbsolutePath()));
				
			} catch (FileNotFoundException e) {
				logger.log(Level.SEVERE, String.format("Could not write the converted output of %1$s to disk as the file couldn't be found!", current_file_name), e);
			} catch (IOException e) {
				logger.log(Level.SEVERE, String.format("Ran into an IO problem writing the new file to disk from the contents of %1$s", current_file_name), e);
			}
			
			
		}

	}

}
